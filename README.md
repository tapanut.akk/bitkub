#How to review my test
1. create your laravel project.
2. Copy my web.php
3. Paste my web.php in your laravel project instead.
4. Run your laravel project on localhost.
5. Copy test link from my web.php and paste it on URL browser.
6. Change parameters in URL by yourself.
7. Review my code and my result by yourself.
8. Call me for an interview via phone.
9. Decided to accept me to work.
10. I can start to work with you after 1 month.
