<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Session 1

// Test 1 Pattern 1
Route::get('/bitkub/1/1/{n}', function ($n) {

    $o_text = '<span style="color:red;">O</span>';
    $x_text = 'X';

    $rows = $n+$n-1;

    for($i=0;$i<$rows;$i++){
        
        if($i>$n-1){
            $col =($n-1)-($i-($n-1));
        } else {
            $col = $i;
        }

        $count = $col;

        for($j=0;$j<$rows;$j++){

            if($j==$count){
                if($count>($rows-1)-$col && $count<=$rows-1){
                    echo($o_text);
                } else {
                    echo($x_text);
                }
                $count+=2;
            } else {
                echo($o_text);
            }
            
            if($j==$rows-1){
                echo("<br>");
            }

        }

    }
    
});

// Test 1 Pattern 2
Route::get('/bitkub/1/2/{n}', function ($n) {

    $o_text = '<span style="color:red;">O</span>';
    $x_text = 'X';

    $max_side = ceil($n/2);
    $even_flag = false;
    if($n%2==0){
        $even_flag = true;
    }

    for($i=0;$i<$n;$i++){

        $x_row = $i+1;
        if($x_row>$max_side){
            if($even_flag){
                $more_side = $x_row-$max_side;
                if($more_side==1){
                    $x_row = $max_side;
                } else {
                    $x_row = $max_side-($more_side-1);
                }
                
            } else {
                $x_row = $max_side-($x_row-$max_side);
            }
        }

        for($j=0;$j<$n;$j++){

            if($j>=0 && $j<=$x_row-1){
                echo($x_text);
            } elseif($j>($n-1-$x_row) && $j<=$n-1) {
                echo($x_text);
            } else {
                echo($o_text);
            }
            
            if($j==$n-1){
                echo("<br>");
            }

        }

    }
    
});

// Test 1 Pattern 3
Route::get('/bitkub/1/3/{n}', function ($n) {

    $y_format = 0;
    $y_text = '<span style="color:red;">Y</span>';
    $x_text = 'X';

    for($i=0;$i<$n;$i++){

        for($j=0;$j<$n;$j++){

            if($i==1&&$i<$n-1){
                if($j>=0&&$j<=$n-2){
                    echo($y_text);
                } else {
                    echo($x_text);
                }
            } elseif($i==2&&$i<$n-2) {
                if($j==$n-2){
                    echo($y_text);
                } else {
                    echo($x_text);
                }
            } elseif($i==3&&$n>6) {
                if(($j>0&&$j<$n-3)||$j==$n-2){
                    echo($y_text);
                } else {
                    echo($x_text);
                }
            } elseif($i>2&&$i<$n-2) {
                if($j==1||$j==$n-2){
                    echo($y_text);
                } else {
                    echo($x_text);
                }
            } elseif($i==$n-2) {
                if($j>0&&$j<$n-1){
                    echo($y_text);
                } else {
                    echo($x_text);
                }
            }else {
                echo($x_text);
            }
            

            if($j==$n-1){
                echo("<br>");
            }

        }

    }
    
});

// Test 2
Route::get('/bitkub/2/{d}/{m}/{y}', function ($d,$m,$y) {

    // $date = explode('/',$date_string);

    $day['1'] = 'จันทร์';
    $day['2'] = 'อังคาร';
    $day['3'] = 'พุธ';
    $day['4'] = 'พฤหัสบดี';
    $day['5'] = 'ศุกร์';
    $day['6'] = 'เสาร์';
    $day['7'] = 'อาทิตย์';

    $month['01'] = 'มกราคม';
    $month['02'] = 'กุมภาพันธ์';
    $month['03'] = 'มีนาคม';
    $month['04'] = 'เมษายน';
    $month['05'] = 'พฤษภาคม';
    $month['06'] = 'มิถุนายน';
    $month['07'] = 'กรกฎาคม';
    $month['08'] = 'สิงหาคม';
    $month['09'] = 'กันยายน';
    $month['10'] = 'ตุลาคม';
    $month['11'] = 'พฤศจิกายน';
    $month['12'] = 'ธันวาคม';

    return "วัน".$day[date('N', strtotime($m.'/'.$d.'/'.$y))]."ที่ ".$d." ".$month[$m]." ".(intval($y)+543);
    
});

// Test 3
Route::get('/bitkub/3', function () {

    $respond = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users"));

    echo '<style>';
    echo '#users {';
    echo 'font-family: Arial, Helvetica, sans-serif;';
    echo 'border-collapse: collapse;';
    echo 'width: 100%;';
    echo '}';
    echo '';
    echo '#users td, #customers th {';
    echo 'border: 1px solid #ddd;';
    echo 'padding: 8px;';
    echo '}';
    echo '';
    echo '#users tr:nth-child(even){background-color: #f2f2f2;}';
    echo '';
    echo '#users tr:hover {background-color: #ddd;}';
    echo '';
    echo '#users th {';
    echo 'padding-top: 12px;';
    echo 'padding-bottom: 12px;';
    echo 'text-align: left;';
    echo 'background-color: #4CAF50;';
    echo 'color: white;';
    echo '}';
    echo '</style>';

    echo '<script type="application/javascript">';
    echo 'function search() {';
    echo 'var field = document.getElementById("field").value;';
    echo 'var value = document.getElementById("value").value;';
    echo 'var url = \'/bitkub/3\';';
    echo 'if(field!=\'\'&&value!=\'\'){';
    echo 'url += \'/\'+field+\'/\'+value;';
    echo '}';
    echo '';
    echo 'window.location.href = url;';
    echo '}';
    echo '</script>';

    echo '<select id="field" name="field">';
    echo '<option value="">All</option>';
    echo '<option value="name">Name</option>';
    echo '<option value="username">Username</option>';
    echo '</select>';
    echo '<input type="text" id="value" name="value">';
    echo '<input type="submit" onclick="search();">';
    echo '<table id="users">';
    echo '<tr>';
    echo '<th>Name</th>';
    echo '<th>Username</th>';
    echo '<th>Email</th>';
    echo '<th>Address</th>';
    echo '<th>Phone</th>';
    echo '<th>Website</th>';
    echo '<th>company</th>';
    echo '</tr>';

    foreach($respond as $row){
        echo '<tr>';
        echo '<td>'.$row->name.'</td>';
        echo '<td>'.$row->username.'</td>';
        echo '<td>'.$row->email.'</td>';
        echo '<td>'.$row->address->street.' '.$row->address->suite.' '.$row->address->city.' '.$row->address->zipcode.' '.$row->address->geo->lat.' '.$row->address->geo->lng.'</td>';
        echo '<td>'.$row->phone.'</td>';
        echo '<td>'.$row->website.'</td>';
        echo '<td>'.$row->company->name.' '.$row->company->catchPhrase.' '.$row->company->bs.'</td>';
        echo '</tr>';
    }
    
    echo '</table>';
    
});

Route::get('/bitkub/3/{field}/{value}', function ($field,$value) {

    $value = str_replace(" ","%20",$value);
    $respond = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users?$field=$value"));

    echo '<style>';
    echo '#users {';
    echo 'font-family: Arial, Helvetica, sans-serif;';
    echo 'border-collapse: collapse;';
    echo 'width: 100%;';
    echo '}';
    echo '';
    echo '#users td, #customers th {';
    echo 'border: 1px solid #ddd;';
    echo 'padding: 8px;';
    echo '}';
    echo '';
    echo '#users tr:nth-child(even){background-color: #f2f2f2;}';
    echo '';
    echo '#users tr:hover {background-color: #ddd;}';
    echo '';
    echo '#users th {';
    echo 'padding-top: 12px;';
    echo 'padding-bottom: 12px;';
    echo 'text-align: left;';
    echo 'background-color: #4CAF50;';
    echo 'color: white;';
    echo '}';
    echo '</style>';

    echo '<script type="application/javascript">';
    echo 'function search() {';
    echo 'var field = document.getElementById("field").value;';
    echo 'var value = document.getElementById("value").value;';
    echo 'var url = \'/bitkub/3\';';
    echo 'if(field!=\'\'&&value!=\'\'){';
    echo 'url += \'/\'+field+\'/\'+value;';
    echo '}';
    echo '';
    echo 'window.location.href = url;';
    echo '}';
    echo '</script>';

    echo '<select id="field" name="field">';
    echo '<option value="">All</option>';
    echo '<option value="name" ';
        if($field == 'name'){
            echo 'selected';
        }
    echo '>Name</option>';
    echo '<option value="username" ';
        if($field == 'username'){
            echo 'selected';
        }
    echo '>Username</option>';
    echo '</select>';
    echo '<input type="text" id="value" name="value" value='.$value.'>';
    echo '<input type="submit" onclick="search();">';
    echo '<table id="users">';
    echo '<tr>';
    echo '<th>Name</th>';
    echo '<th>Username</th>';
    echo '<th>Email</th>';
    echo '<th>Address</th>';
    echo '<th>Phone</th>';
    echo '<th>Website</th>';
    echo '<th>company</th>';
    echo '</tr>';

    foreach($respond as $row){
        echo '<tr>';
        echo '<td>'.$row->name.'</td>';
        echo '<td>'.$row->username.'</td>';
        echo '<td>'.$row->email.'</td>';
        echo '<td>'.$row->address->street.' '.$row->address->suite.' '.$row->address->city.' '.$row->address->zipcode.' '.$row->address->geo->lat.' '.$row->address->geo->lng.'</td>';
        echo '<td>'.$row->phone.'</td>';
        echo '<td>'.$row->website.'</td>';
        echo '<td>'.$row->company->name.' '.$row->company->catchPhrase.' '.$row->company->bs.'</td>';
        echo '</tr>';
    }
    
    echo '</table>';
    
});
